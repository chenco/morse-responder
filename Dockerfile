FROM alpine:3.12
RUN apk add nmap-ncat
COPY . .
EXPOSE 8080
CMD ["sh", "server.sh"]