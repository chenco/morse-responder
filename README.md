[![pipeline status](https://gitlab.com/chenco/morse-responder/badges/master/pipeline.svg)](https://gitlab.com/chenco/morse-responder/-/commits/master)

# Morse-Responder

Answers client with its morse-decoded ip address.

```
docker build . -t morse-responder
docker run -it --rm -p 8080:8080 morse-responder
curl localhost:8080
.---- --... ..--- / .---- --... / ----- / .----
```